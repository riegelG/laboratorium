module Index

open Elmish
open Fable.Remoting.Client
open Shared
open System

type BookForm = 
    { Title: string
      AuthorId: string }

type AuthorForm = 
    { LastName: string
      FirstName: string }

type Model =
    { Books: Book list 
      Authors: Author list
      Input: string
      BookForm: BookForm
      AuthorForm: AuthorForm }

type Msg =
    | GotBooks of Book list
    | GotAuthors of Author list
    | SetInput of string
    | SetTitle of string
    | SetAuthorId of string
    | SetFirstName of string
    | SetLastName of string
    | RefreshAuthors of unit
    | RefreshBooks of unit
    | DeleteBook of Guid
    | DeleteAuthor of Guid
    | AddAuthor 
    | AddBook
    | UpdateAuthor of Guid 
    | UpdateBook of Guid

let booksApi =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.buildProxy<IBooksApi>

let authorsApi =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.buildProxy<IAuthorsApi>

let init(): Model * Cmd<Msg> =
    printf "INIT"
    let model =
        { Books = []
          Authors = []
          Input = ""
          BookForm = {Title = ""; AuthorId = "" }
          AuthorForm = { FirstName = ""; LastName = "" } }
    
    let cmd = Cmd.OfAsync.perform booksApi.getList () GotBooks
    model, cmd

let update (msg: Msg) (model: Model): Model * Cmd<Msg> =
    match msg with
    | GotBooks books ->
        let cmd = Cmd.OfAsync.perform authorsApi.getList () GotAuthors
        { model with Books = books }, cmd
    | GotAuthors authors ->
        { model with Authors = authors}, Cmd.none
    | SetInput value ->
        { model with Input = value }, Cmd.none
    | SetFirstName value ->
        { model with AuthorForm = { model.AuthorForm with FirstName = value }}, Cmd.none
    | SetLastName value ->
        { model with AuthorForm = { model.AuthorForm with LastName = value }}, Cmd.none
    | SetTitle value ->
        { model with BookForm = { model.BookForm with Title = value }}, Cmd.none
    | SetAuthorId value ->
        { model with BookForm = { model.BookForm with AuthorId = value }}, Cmd.none
    | AddAuthor ->
        let newAuthor = Author.create model.AuthorForm.FirstName model.AuthorForm.LastName
        let cmd = Cmd.OfAsync.perform authorsApi.add newAuthor RefreshAuthors
        { model with AuthorForm = { FirstName = ""; LastName = "" } }, cmd
    | AddBook ->
        let newBook = Book.create model.BookForm.Title (Guid.Parse model.BookForm.AuthorId)
        let cmd = Cmd.OfAsync.perform booksApi.add newBook RefreshBooks
        { model with BookForm = { Title = ""; AuthorId = "" } }, cmd
    | UpdateAuthor id ->
        let author = Author.create model.AuthorForm.FirstName model.AuthorForm.LastName
        let cmd = Cmd.OfAsync.perform authorsApi.update {author with Id = id} RefreshAuthors
        { model with AuthorForm = { FirstName = ""; LastName = "" } }, cmd
    | UpdateBook id ->
        let book = Book.create model.BookForm.Title (Guid.Parse model.BookForm.AuthorId)
        let cmd = Cmd.OfAsync.perform booksApi.update {book with Id = id} RefreshBooks
        { model with BookForm = { Title = ""; AuthorId = "" } }, cmd
    | DeleteAuthor id ->
        let cmd = Cmd.OfAsync.perform authorsApi.delete id RefreshAuthors
        model, cmd
    | DeleteBook id ->
        let cmd = Cmd.OfAsync.perform booksApi.delete id RefreshBooks
        model, cmd
    | RefreshAuthors _ ->
        let cmd = Cmd.OfAsync.perform authorsApi.getList () GotAuthors
        model, cmd
    | RefreshBooks _ ->
        let cmd = Cmd.OfAsync.perform booksApi.getList () GotBooks
        model, cmd

open Fable.React
open Fable.React.Props
open Fulma

let navBrand =
    Navbar.Brand.div [ ] [
        Navbar.Item.a [
            Navbar.Item.Props [ Href "https://safe-stack.github.io/" ]
            Navbar.Item.IsActive true
        ] [
            img [
                Src "/favicon.png"
                Alt "Logo"
            ]
        ]
    ]

let containerBox (model : Model) (dispatch : Msg -> unit) =
    Box.box' [ ] [
        Content.content [ ] [
            Control.p [ ] [ str "Books" ] 
            Content.Ol.ol [ ] [
                for book in model.Books do
                    li [ ] [
                        Field.div [ Field.IsGrouped ] [ 
                            Control.p [ ] [ str book.Title ] 
                            Button.a [
                                Button.Color IsPrimary
                                Button.Disabled (Book.isValid model.BookForm.Title |> not)
                                Button.OnClick (fun _ -> dispatch (UpdateBook book.Id))
                            ] [
                                str "Update"
                            ]
                            Button.a [
                                Button.Color IsPrimary
                                Button.OnClick (fun _ -> dispatch (DeleteBook book.Id))
                            ] [
                                str "Remove"
                            ]
                        ]
                    ]
            ]
            Control.p [ ] [ str "Authors" ] 
            Content.Ol.ol [ ] [
                for author in model.Authors do
                    li [ ] [ 
                        Field.div [ Field.IsGrouped ] [ 
                            Control.p [ ] [ str (Author.getFullName author) ] 
                            Button.a [
                                Button.Color IsPrimary
                                Button.OnClick (fun _ -> dispatch (UpdateAuthor author.Id))
                                Button.Disabled (Author.isValid model.AuthorForm.FirstName model.AuthorForm.LastName |> not)

                            ] [
                                str "Update"
                            ]
                            Button.a [
                                Button.Color IsPrimary
                                Button.OnClick (fun _ -> dispatch (DeleteAuthor author.Id))
                            ] [
                                str "Remove"
                            ]
                        ]
                    ]
            ]
        ]
        Field.div [ Field.IsGrouped ] [
            Control.p [ Control.IsExpanded ] [
                Input.text [
                  Input.Value model.BookForm.Title
                  Input.Placeholder "Title"
                  Input.OnChange (fun x -> SetTitle x.Value |> dispatch) ]
            ]
            Control.p [ Control.IsExpanded ] [
                select [
                    Value model.BookForm.AuthorId
                    Placeholder "AuthorId"
                    OnChange (fun x -> SetAuthorId x.Value |> dispatch) ] 
                    [ 
                        for a in model.Authors do
                            option [ Value (a.Id.ToString()) ] [ str (Author.getFullName a) ]
                    ]
            ]
            Control.p [ ] [
                Button.a [
                    Button.Color IsPrimary
                    Button.Disabled (Book.isValid model.BookForm.Title |> not)
                    Button.OnClick (fun _ -> dispatch AddBook)
                ] [
                    str "Add Book"
                ]
            ]
        ]
        Field.div [ Field.IsGrouped ] [
            Control.p [ Control.IsExpanded ] [
                Input.text [
                  Input.Value model.AuthorForm.FirstName
                  Input.Placeholder "First name"
                  Input.OnChange (fun x -> SetFirstName x.Value |> dispatch) ]
            ]
            Control.p [ Control.IsExpanded ] [
                Input.text [
                  Input.Value model.AuthorForm.LastName
                  Input.Placeholder "Last name"
                  Input.OnChange (fun x -> SetLastName x.Value |> dispatch) ]
            ]
            Control.p [ ] [
                Button.a [
                    Button.Color IsPrimary
                    Button.Disabled (Author.isValid model.AuthorForm.FirstName model.AuthorForm.LastName |> not)
                    Button.OnClick (fun _ -> dispatch AddAuthor)
                ] [
                    str "Add Author"
                ]
            ]
        ]
    ]

let view (model : Model) (dispatch : Msg -> unit) =
    Hero.hero [
        Hero.Color IsPrimary
        Hero.IsFullHeight
        Hero.Props [
            Style [
                Background """linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("https://unsplash.it/1200/900?random") no-repeat center center fixed"""
                BackgroundSize "cover"
            ]
        ]
    ] [
        Hero.head [ ] [
            Navbar.navbar [ ] [
                Container.container [ ] [ navBrand ]
            ]
        ]

        Hero.body [ ] [
            Container.container [ ] [
                Column.column [
                    Column.Width (Screen.All, Column.Is6)
                    Column.Offset (Screen.All, Column.Is3)
                ] [
                    Heading.p [ Heading.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] [ str "standard" ]
                    containerBox model dispatch
                ]
            ]
        ]
    ]
