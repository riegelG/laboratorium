namespace Db 
open Shared

#r @".\bin\Debug\netcoreapp3.1\FSharp.Data.SqlProvider.dll"
#r @".\bin\Debug\netcoreapp3.1\Npgsql.dll"
open FSharp.Data.Sql
open System

module Db =
    [<Literal>]
    let connectionString = @"Host=localhost;Port=5432;Database=db;Username=postgres;Password=admin;"

    type Sql = SqlDataProvider<Common.DatabaseProviderTypes.POSTGRESQL, connectionString, UseOptionTypes=true>
    type DbContext = Sql.dataContext
    let ctx: DbContext = Sql.GetDataContext connectionString

    //Tabela `authors`
    module Authors =

        let private mapAuthor (dbRecord:DbContext.``public.authorsEntity``) : Author =
            { Id = dbRecord.Id
              FirstName = dbRecord.FirstName
              LastName = dbRecord.LastName }

        let getAll (ctx: DbContext): Author list = 
            ctx.Public.Authors |> Seq.map (fun x -> x.MapTo<Author>()) |> Seq.toList

        let get (ctx: DbContext) (id: Guid): Author option =  
            query {
                for author in ctx.Public.Authors do
                    where (author.Id = id)
                    select author
            } 
            |> Seq.map (fun x -> x.MapTo<Author>()) 
            |> Seq.tryHead

        let update (author: Author): unit =
            query {
                for a in ctx.Public.Authors do
                    where (a.Id = author.Id)
                    select a
            } 
            |> Seq.iter (fun row -> 
                row.FirstName <- author.FirstName
                row.LastName <- author.LastName
            )
            ctx.SubmitUpdates()

        let add (author: Author): unit =
            let newAuthor = ctx.Public.Authors.Create()
            newAuthor.Id <- author.Id
            newAuthor.FirstName <- author.FirstName
            newAuthor.LastName <- author.LastName
            ctx.SubmitUpdates()

        let delete (id: Guid): unit =
            let ctx: DbContext = Sql.GetDataContext connectionString
            query {
                for author in ctx.Public.Authors do
                    where (author.Id = id)
                    select author
            } 
            |> Seq.iter (fun row -> row.Delete())
            ctx.SubmitUpdates()

    //Tabela `books`
    module Books =
        let private books = ctx.Public.Books

        type BookEntity = DbContext.``public.booksEntity``

        let mapBook (dbRecord:DbContext.``public.booksEntity``) : Book =
            { Id = dbRecord.Id
              Title = dbRecord.Title
              AuthorId = dbRecord.AuthorId }

        let getAll (ctx: DbContext): Book list = books |> Seq.map (fun x -> x.MapTo<Book>()) |> Seq.toList

        let get (ctx: DbContext) (id: Guid): Book option = 
            query {
                for book in books do
                    where (book.Id = id)
                    select book
            } 
            |> Seq.map (fun x -> x.MapTo<Book>())
            |> Seq.tryHead

        let update (book: Book): unit =
            query {
                for b in books do
                    where (b.Id = book.Id)
                    select b
            } 
            |> Seq.iter (fun row -> 
                row.Title <- book.Title
                row.AuthorId <- book.AuthorId
            )
            ctx.SubmitUpdates()

        let add (book: Book): unit =
            let newBook = books.Create()
            newBook.Id <- book.Id
            newBook.Title <- book.Title
            newBook.AuthorId <- book.AuthorId
            ctx.SubmitUpdates()

        let delete (id: Guid): unit =
            query {
                for book in books do
                    where (book.Id = id)
                    select book
            } 
            |> Seq.iter (fun row -> row.Delete())
            ctx.SubmitUpdates()