module Server

open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open Giraffe
open Saturn
open System
open Shared
open Db


let authorsApi =
    {
        getList = fun () -> async { return Db.Authors.getAll Db.ctx }
        get = fun id -> async { return Db.Authors.get Db.ctx id }
        delete = fun id -> async { return Db.Authors.delete id }
        add = fun author -> async { return Db.Authors.add author }
        update = fun author -> async { return Db.Authors.update author }
    }

let booksApi: IBooksApi =
    {
        getList = fun () -> async { return Db.Books.getAll Db.ctx }
        get = fun id -> async { return Db.Books.get Db.ctx id }
        delete = fun (id: Guid) -> async { return Db.Books.delete id }
        add = fun book -> async { return Db.Books.add book }
        update = fun book -> async { return Db.Books.update book }
    }

let booksApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue booksApi
    |> Remoting.buildHttpHandler

let authorsApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue authorsApi
    |> Remoting.buildHttpHandler

let webApp = choose [
    booksApp
    authorsApp]

let app =
    application {
        url "http://0.0.0.0:8085"
        use_router webApp
        memory_cache
        use_static "public"
        use_gzip
    }

run app
