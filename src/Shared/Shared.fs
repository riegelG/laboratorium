namespace Shared

open System

type Author =
    {
        Id: Guid
        FirstName: string
        LastName: string
    }

type Book =
    { Id: Guid
      Title: string
      AuthorId: Guid }

module Author =
    let private isValidFirstName (firstName: string) =
        String.IsNullOrWhiteSpace firstName |> not

    let private isValidLastName (lastName: string) =
        String.IsNullOrWhiteSpace lastName |> not

    let isValid (firstName: string) (lastName: string) =
        isValidFirstName firstName && isValidLastName lastName

    let create (firstName: string) (lastName: string) =
        {
            Id = Guid.NewGuid()
            FirstName = firstName
            LastName = lastName
        }

    let getFullName (author: Author) = author.FirstName + " " + author.LastName


module Book =
    let private isValidTitle (title: string) =
        String.IsNullOrWhiteSpace title |> not

    let isValid (title: string) =
        isValidTitle title

    let create (title: string) (authorId: Guid) =
        {
            Id = Guid.NewGuid()
            Title = title
            AuthorId = authorId
        }

module Route =
    let builder typeName methodName =
        sprintf "/api/%s/%s" typeName methodName

type IBooksApi =
    {
        getList : unit -> Async<Book list>
        get : Guid -> Async<Book option>
        delete : Guid -> Async<unit>
        add : Book -> Async<unit>
        update : Book -> Async<unit>
    }

type IAuthorsApi =
    {
        getList : unit -> Async<Author list>
        get : Guid -> Async<Author option>
        delete : Guid -> Async<unit>
        add : Author -> Async<unit>
        update : Author -> Async<unit>
    }